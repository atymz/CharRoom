create database chat;

create table user
(
    id         int auto_increment
        primary key,
    name       varchar(20) null,
    password   varchar(10) null,
    created_at timestamp   null,
    updated_at timestamp   null,
    constraint user_id_uindex
        unique (id)
)
    comment '用户表';

INSERT INTO chat.user (id, name, password, created_at, updated_at)
VALUES (1, 'admin', '123', '2022-06-30 14:21:06', '2022-06-30 14:21:09');
INSERT INTO chat.user (id, name, password, created_at, updated_at)
VALUES (2, 'guest', '123', '2022-06-30 14:21:32', '2022-06-30 14:21:35');
INSERT INTO chat.user (id, name, password, created_at, updated_at)
VALUES (3, 'zhangsan', '123', null, null);
INSERT INTO chat.user (id, name, password, created_at, updated_at)
VALUES (5, 'lisi', '123', '2022-07-05 14:34:41', '2022-07-05 14:34:41');


create table room
(
    id         int auto_increment
        primary key,
    creator_id int                  null comment '创建人ID',
    room_name  varchar(10)          null comment '房间名',
    created_at timestamp            null comment '创建时间',
    is_exist   tinyint(1) default 1 null comment '是否存在',
    constraint Room_id_uindex
        unique (id)
)
    comment '房间表';

create table message
(
    id        int auto_increment
        primary key,
    room_id   int          null comment '房间id',
    user_id   int          null comment '用户id',
    msg       varchar(500) null comment '聊天内容',
    send_time timestamp    null comment '发送时间',
    ip        char(15)     null comment 'IP地址',
    constraint msg_history_id_uindex
        unique (id)
)
    comment '历史消息表';
