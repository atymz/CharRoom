from flask_cors import CORS

from app import app, socketio

CORS(app, supports_credentials=True)

if __name__ == '__main__':
    # app.debug = True
    # app.run(host='0.0.0.0', port=80)
    socketio.run(app, host='127.0.0.1', port=80, debug=True)
