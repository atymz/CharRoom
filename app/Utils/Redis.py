import json

from app import redis_cli


class Redis:
    # 定义记录个数
    RECORD_NUM = 100

    # 保存RECORD_NUM条消息记录
    @classmethod
    def save_message(cls, message):
        # 创建key
        key = "chat_" + message["room"]
        if redis_cli.exists(key):
            if redis_cli.llen(key) >= cls.RECORD_NUM:
                redis_cli.lpop(key)
            message = json.dumps(message)
            redis_cli.rpush(key, message)

    # 获取消息
    @classmethod
    def get_message(cls, room) -> list:
        # 创建key
        key = "chat_" + room
        # 获取消息
        messages = redis_cli.lrange(key, 0, -1)
        if messages:
            messages = [json.loads(message) for message in messages]
            return messages
        else:
            return []

    # 删除消息
    @classmethod
    def delete_message(cls, room):
        # 创建key
        key = "chat_" + room
        # 删除消息
        redis_cli.delete(key)


if __name__ == '__main__':
    Redis.save_message({
        "username": "zhangsan",
        "message": "hello",
        "room": "default",
        "time": "2020-01-01 12:00:00"
    })
    result = Redis.get_message("default")
    print(type(result[0]), result[0])
