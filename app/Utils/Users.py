from typing import Optional

from app import dBSession
from app.Model import User


class Users:
    # 获取单个用户的信息
    @staticmethod
    def get_by_user(username: str) -> Optional[User]:
        _user = dBSession.query(User).filter_by(name=username).first()
        if _user is None:
            return None
        else:
            return _user

    # 增加用户
    @staticmethod
    def add(_user: User) -> bool:
        dBSession.add(_user)
        dBSession.commit()
        return True

    # 根据id删除用户
    @staticmethod
    def delete(_id: int) -> bool:
        try:
            dBSession.query(User).filter_by(id=_id).delete()
            dBSession.commit()
            return True
        except Exception as e:
            print(e)
            return False

    # 根据id修改用户
    @staticmethod
    def update(_id: int, user: User) -> bool:
        try:
            dBSession.query(User).filter_by(id=_id).update(user)
            dBSession.commit()
            return True
        except Exception as e:
            print(e)
            return False


if __name__ == '__main__':
    user = Users.get_by_user('admin')
    print(user.to_dict())
