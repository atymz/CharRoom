from flask import session
from flask_socketio import emit, join_room, leave_room

from app import socketio
from app.Utils.Redis import Redis


# 连接
@socketio.on('connect', namespace='/chat')
def connect():
    result = Redis.get_message('default')  # 暂时
    emit('connect', result, namespace='/chat')
    print('已连接客户端')


# 断开
@socketio.on('disconnect', namespace='/chat')
def disconnect():
    # leave_room(session['username'])
    result = {
        'name': session['username'],
    }
    emit('leave', result, namespace='/chat')
    print('已断开客户端连接')


# 接收->保存->发送
@socketio.on('receive', namespace='/chat')
def receive(data):
    print(data)
    Redis.save_message(data)
    result = {
        'username': data['username'],
        'message': data['message']
    }
    emit('broadcast', result, namespace='/chat', broadcast=True)


# 进入房间->恢复历史消息->发送
@socketio.on('join', namespace='/chat')
def join(message):
    print('==-------join ----------==')
    print(session.items())
    username = message['username']
    room = message['room']
    join_room(room)
    # session['receive_count'] = session.get('receive_count', 0) + 1
    result = {
        'username': username,
        'room': room
    }
    emit('message', result, room=room, namespace='/chat')


# 离开房间
@socketio.on('leave', namespace='/chat')
def leave(message):
    name = message['name']
    room = message['room']
    leave_room(room)
    result = {
        'name': name,
        'room': room
    }
    emit('leave', result, room=room, namespace='/chat')
