from datetime import datetime

from flask import redirect, url_for, request, session
from flask import render_template

from app import app, socketio
from app.Model import User
from app.Utils.Users import Users


@app.route('/', methods=['GET'])
def index():
    return redirect(url_for('login'))


# 登录
@app.route('/login', methods=['GET', 'POST'])
def login():
    if request.method == 'GET':
        return render_template('login.html')
    elif request.method == 'POST':
        username = request.form.get('username')
        password = request.form.get('password')
        if username and password:
            user = Users.get_by_user(username)
            if user and user.password == password:
                session['username'] = user.name
                return redirect(url_for('chat'))
            else:
                return redirect(url_for('login'))

    return render_template('login.html')


# 注册
@app.route('/register', methods=['GET', 'POST'])
def register():
    if request.method == 'GET':
        return render_template('register.html')
    elif request.method == 'POST':
        username = request.form.get('username')
        password = request.form.get('password')
        if username and password:
            user = Users.get_by_user(username)
            if user is None:
                user = User(name=username, password=password, created_at=datetime.now(), updated_at=datetime.now())
                Users.add(user)
                return redirect(url_for('login'))
            else:
                return render_template('register.html', error='用户已存在')
        else:
            return render_template('register.html', error='用户名或密码为空')


# 去聊天
@app.route('/chat', methods=['GET'])
def chat():
    if 'username' in session:
        result = Users.get_by_user(session['username'])
        # result = jsonify(result.to_dict())
        result = result.to_dict()
        return render_template('chat.html', async_mode=socketio.async_mode, result=result)
    else:
        return redirect(url_for('login'))
