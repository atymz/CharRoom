# coding: utf-8
from sqlalchemy import CHAR, Column, Integer, String, TIMESTAMP, text
from sqlalchemy.dialects.mysql import TINYINT
from sqlalchemy.ext.declarative import declarative_base

Base = declarative_base()
metadata = Base.metadata


class Message(Base):
    __tablename__ = 'message'
    __table_args__ = {'comment': '聊天记录'}

    id = Column(Integer, primary_key=True, unique=True)
    room_id = Column(Integer, comment='房间id')
    user_id = Column(Integer, comment='用户id')
    msg = Column(String(500), comment='聊天内容')
    send_time = Column(TIMESTAMP, comment='发送时间')
    ip = Column(CHAR(15), comment='IP地址')


class Room(Base):
    __tablename__ = 'room'
    __table_args__ = {'comment': '房间表'}

    id = Column(Integer, primary_key=True, unique=True)
    creator_id = Column(Integer, comment='创建人ID')
    room_name = Column(String(10), comment='房间名')
    created_at = Column(TIMESTAMP, comment='创建时间')
    is_exist = Column(TINYINT(1), server_default=text("'1'"), comment='是否存在')


class User(Base):
    __tablename__ = 'user'
    __table_args__ = {'comment': '用户表'}

    id = Column(Integer, primary_key=True, unique=True)
    name = Column(String(20))
    password = Column(String(10))
    created_at = Column(TIMESTAMP)
    updated_at = Column(TIMESTAMP)

    def to_dict(self):
        return {
            'id': self.id,
            'name': self.name,
            'password': self.password,
            'created_at': self.created_at.strftime('%Y-%m-%d %H:%M:%S') if self.created_at else '',
            'updated_at': self.updated_at.strftime('%Y-%m-%d %H:%M:%S') if self.updated_at else ''
        }


if __name__ == '__main__':
    print('aaa')
