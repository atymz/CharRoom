from flask import Flask
from flask_socketio import SocketIO
from redis import StrictRedis
from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker

# 连接Redis
redis_cli = StrictRedis(host="127.0.0.1", port=6379, db=0)
# 要指定模板文件和静态文件的位置【坑】
app = Flask(__name__, template_folder='../templates', static_folder='../static')
# 将模板{{}}改为[[]]
# app.jinja_env.variable_start_string = '[['
# app.jinja_env.variable_end_string = ']]'
# 实例化websocket
async_mode = None  # 设置异步模式，可选"threading", "eventlet" , "gevent"
app.config['SECRET_KEY'] = 'secret!'
socketio = SocketIO(app, async_mode=async_mode)
# 设置数据库配置
app.config['SQLALCHEMY_DATABASE_URI'] = 'mysql+pymysql://root:root@localhost:3306/chat?charset=utf8'
# 是否跟踪修改
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False
# 创建数据库及连接
engine = create_engine(app.config['SQLALCHEMY_DATABASE_URI'])  # , echo=True)
# 创建DBSession类型:
DBSession = sessionmaker(bind=engine)
dBSession = DBSession()
# 引入使用的控制器
from app.View import UsersView, SocketView

# 引入蓝图
# from app.View.RestfulController import restful


# result = dBSession.execute("select * from user")
# print(result)
