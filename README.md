# 项目简介

ChatRoom是一个简单的聊天室程序，用到的技术有Python,MySQL,Redis。

# 项目结构

- app 包含了聊天室的所有代码
    - `__init__.py` 项目初始化
    - Model.py 数据模型，把数据库表映射成对象
    - View 视图层，处理RestfulAPI和SocketIO的请求
    - Utils 工具类，提供操作User表和Redis的方法
- static 一些前端配置
- templates 网页
- initDB.sql 初始化数据库
- requirements.txt 安装依赖
- run.py 项目入口文件

```shell
pip install -r requirements.txt
```
