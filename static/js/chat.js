$(document).ready(function () {
    let time = new Date().getTime()
    namespace = '/chat';
    // 创建websocket连接
    let socket = io('http://127.0.0.1' + namespace);
    socket.on('connect', function (history_message) {
        // socket.emit('my_event', {data: 'Connected to server'});
        console.log('=============已成功连接=============');
        console.log(history_message);

        // 恢复历史消息
        if (history_message !== undefined) {
            for (let h_s of history_message) {
                let msg = h_s.message
                let username = h_s.username
                if (h_s.username === $('#msgbox_name').html()) {
                    let item_right = document.createElement('div');
                    item_right.className = 'item item-right';
                    item_right.innerHTML = `
                <div id = 'personB' class="bubble bubble-right">${msg}</div>
                <div class="avatar"><img src="../static/img/userB.jpg" /></div>
              `;
                    document.querySelector('.content').appendChild(item_right);
                } else {
                    let item_left = document.createElement('div');
                    item_left.className = 'item item-left';
                    item_left.innerHTML = `
                    <div class="item item-left" id='personA'>
                        <div class="avatar">
                            <img src="../static/img/userA.jpg"/>
                        </div>
                        <div class="bubble bubble-left">${username}:${msg}</div>
                    </div>
                  `;
                    document.querySelector('.content').appendChild(item_left);
                }
            }
        }
    });


    socket.emit('join', {username: $('#msgbox_name').html(), room: 'default'});

    // 显示“进入房间”
    socket.on('message', function (data) {
        console.log("=============JOIN============")
        console.log(data)

        // 显示当前用户进入房间
        let item = document.createElement('div');
        item.className = 'item item-center';
        item.innerHTML = `<div class="item item-center"><span>欢迎${data.username}进入房间</span></div>`;
        document.querySelector('.content').appendChild(item);
    });
    // 显示“离开房间”
    socket.on('leave', function (data) {
        console.log("=============LEAVE============")
        console.log(data)
        let item = document.createElement('div');
        item.className = 'item item-center';
        item.innerHTML = `<div class="item item-center"><span>${data.username}离开房间</span></div>`;
        document.querySelector('.content').appendChild(item);
    });
    // 获得文本框焦点
    document.querySelector('#textarea').focus();
    // 滚动条置底
    document.querySelector(".content").scrollTop = document.querySelector('.content').scrollHeight;

    // 收听聊天消息
    socket.on('broadcast', function (data) {
        console.log("=============BROADCAST============")
        console.log(data)
        if (data.username !== $('#msgbox_name').html()) {
            console.log(data.username, $('#msgbox_name').html())
            let item_left = document.createElement('div');
            item_left.className = 'item item-left';
            item_left.innerHTML = `
            <div class="item item-left" id='personA'>
                <div class="avatar">
                    <img src="../static/img/userA.jpg"/>
                </div>
                <div class="bubble bubble-left">${data.username}:${data.message}</div>
            </div>
          `;
            document.querySelector('.content').appendChild(item_left);
            // 滚动条置底
            document.querySelector(".content").scrollTop = document.querySelector('.content').scrollHeight;
        }
    });


    $('#send-btn').click(function () {
        let text = document.querySelector('#textarea').value;
        text = text.trim();
        if (!text) {
            alert('请输入内容');
            return;
        }
        // 发送消息
        const data = {
            username: $('#msgbox_name').html(),
            message: text,
            time: new Date().toLocaleString(),
            room: 'default'
        };
        socket.emit('receive', data);


        // 如果消息回复超两分钟就加上时间标签
        let now = new Date().getTime()
        if (now - time > 1000 * 60 * 2) {
            let item = document.createElement('div');
            item.className = 'item item-center';
            item.innerHTML = `<div class="item item-center"><span>${new Date(now).format("yyyy-MM-dd hh:mm:ss")}</span></div>`;
            document.querySelector('.content').appendChild(item);
            time = now;
        }


        // 消息上屏
        let item = document.createElement('div');
        item.className = 'item item-right';
        item.innerHTML = `
            <div id = 'personB' class="bubble bubble-right">${text}</div>
            <div class="avatar"><img src="../static/img/userB.jpg" /></div>
        `;
        document.querySelector('.content').appendChild(item);
        // 清空文本框并获得焦点
        document.querySelector('#textarea').value = '';
        document.querySelector('#textarea').focus();
        // 滚动条置底
        document.querySelector(".content").scrollTop = document.querySelector('.content').scrollHeight;
    });

    // 给发送按钮绑定回车
    $('#textarea').keydown(function (event) {
        if (event.keyCode === 13) {
            $('#send-btn').click();
        }
    });


});


Date.prototype.format = function (fmt) {
    var o = {
        "M+": this.getMonth() + 1,                 //月份
        "d+": this.getDate(),                    //日
        "h+": this.getHours(),                   //小时
        "m+": this.getMinutes(),                 //分
        "s+": this.getSeconds(),                 //秒
        "q+": Math.floor((this.getMonth() + 3) / 3), //季度
        "S": this.getMilliseconds()             //毫秒
    };

    if (/(y+)/.test(fmt)) {
        fmt = fmt.replace(RegExp.$1, (this.getFullYear() + "").substr(4 - RegExp.$1.length));
    }

    for (var k in o) {
        if (new RegExp("(" + k + ")").test(fmt)) {
            fmt = fmt.replace(
                RegExp.$1, (RegExp.$1.length == 1) ? (o[k]) : (("00" + o[k]).substr(("" + o[k]).length)));
        }
    }

    return fmt;
}
